import java.io.File

fun main(args: Array<String>) {
    println("AoC - Day2 - RPS\n------------------------")
    val rounds = createListOfRoundsFromInput()
    val totalScorePartOne = evaluateTotalScoreFromStrategyGuidePartOne(rounds)
    val totalScorePartTwo = evaluateTotalScoreFromStrategyGuidePartTwo(rounds)
    
    // task 1 result
    println("Total score after all rounds: $totalScorePartOne")
    // task 2 result
    println("Total score after all rounds: $totalScorePartTwo")
}

private fun createListOfRoundsFromInput(): MutableList<Pair<Char, Char>> {
    val listOfPlays = mutableListOf<Pair<Char, Char>>()
    val filePath = "src/main/resources/strategyGuideEncr.txt"
    File(filePath).forEachLine { line ->
        val opponentPlay = line.split(" ")[0][0]
        val myPlay = line.split(" ")[1][0]
        val round = Pair(opponentPlay, myPlay)
        listOfPlays.add(round)
    }
    return listOfPlays
}

private fun evaluateTotalScoreFromStrategyGuidePartOne(rounds: MutableList<Pair<Char, Char>>): Int {
    var totalScore = 0
    rounds.forEach { round ->
        totalScore += evaluateScoreForPlayedHandPartOne(round.second)
        totalScore += evaluateScoreForOutComePartOne(round)
    }
    return totalScore
}

private fun evaluateScoreForPlayedHandPartOne(play: Char): Int {
    return when(play) {
        'X' -> 1
        'Y' -> 2
        'Z' -> 3
        else -> throw IllegalArgumentException("Invalid input: $play")
    }
}

private fun evaluateScoreForOutComePartOne(round: Pair<Char, Char>): Int {
    return when (round.first) {
        'A' -> when (round.second) {
            'X' -> 3
            'Y' -> 6
            'Z' -> 0
            else -> throw IllegalArgumentException("Invalid input: ${round.second}")
        }
        'B' -> when (round.second) {
            'X' -> 0
            'Y' -> 3
            'Z' -> 6
            else -> throw IllegalArgumentException("Invalid input: ${round.second}")
        }
        'C' -> when (round.second) {
            'X' -> 6
            'Y' -> 0
            'Z' -> 3
            else -> throw IllegalArgumentException("Invalid input: ${round.second}")
        }
        else -> throw IllegalArgumentException("Invalid input: ${round.first}")
    }
}

private fun evaluateTotalScoreFromStrategyGuidePartTwo(rounds: MutableList<Pair<Char, Char>>): Int {
    var totalScore = 0
    rounds.forEach { round ->
        totalScore += evaluateScoreForOutcomePartTwo(round.second)
        totalScore += evaluateScoreForPlayedHandPartTwo(round)
    }
    return totalScore
}

private fun evaluateScoreForOutcomePartTwo(outcome: Char): Int {
    return when(outcome) {
        'X' -> 0
        'Y' -> 3
        'Z' -> 6
        else -> throw Exception()
    }
}

private fun evaluateScoreForPlayedHandPartTwo(round: Pair<Char, Char>): Int {
    return when (round.first) {
        'A' -> when (round.second) {
            'X' -> 3 // play scissors -> 3 pts
            'Y' -> 1 // play rock -> 1 pt
            'Z' -> 2 // play paper -> 2 pts
            else -> throw IllegalArgumentException("Invalid input: ${round.second}")
        }
        'B' -> when (round.second) {
            'X' -> 1 // play rock -> 1pt
            'Y' -> 2 // play paper -> 2pts
            'Z' -> 3 // play scissors -> 3pts
            else -> throw IllegalArgumentException("Invalid input: ${round.second}")
        }
        'C' -> when (round.second) {
            'X' -> 2 // play paper -> 2pts
            'Y' -> 3 // play scissors -> 3pts
            'Z' -> 1 // play rock -> 1 pt
            else -> throw IllegalArgumentException("Invalid input: ${round.second}")
        }
        else -> throw IllegalArgumentException("Invalid input: ${round.first}")
    }
}